from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/',  methods = ['POST', 'GET'])
def index():
    answer = ''
    if request.method == 'POST':
        x = request.form.get('x', type=int)
        y = request.form.get('y', type=int)
        answer = plus(x,y)

    return render_template('form.html', answer=answer)


def plus(x,y):
    answer = x + y
    return answer


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=80, debug=True)
